<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Type;
use App\Entity\User;
use App\Entity\Property;
use App\DataFixtures\TypeFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class AppFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $fixture)
    {

        // init faker
        $faker = Factory::create('fr_FR');

        //create 1 admin
        for ($i = 0; $i < 1; $i++) {
            $admin = new User();
            $admin->setName('admin');
            $admin->setRole('admin');
            $admin->setEmail('admin@campingespadrillevolante.fr');
            $admin->setPassword('admin');

            $fixture->persist($admin);
        }


        //create 10 manager 
        for ($i = 0; $i < 10; $i++) {
            $userManager = new User();
            $userManager->setName($faker->name);
            $userManager->setRole('manager');
            $userManager->setEmail(str_replace(' ', '', ($userManager->name.'@campingespadrillevolante.fr')));
            $userManager->setPassword('password');

            $fixture->persist($userManager);
        }

        //create 30 proprietaire 
        for ($i = 0; $i < 30; $i++) {
            $proprietaire = new User();
            $proprietaire->setName($faker->name);
            $proprietaire->setRole('propriétaire');
            $proprietaire->setEmail(str_replace(' ', '', ($proprietaire->name.'@'. $faker->freeEmailDomain)));
            $proprietaire->setPassword('password');

            $fixture->persist($proprietaire);
        }

        // create 40 user
        for ($i = 1; $i < 40; $i++) {
            $user = new User();
            $user->setName($faker->name);
            $user->setRole('user');
            $user->setEmail(str_replace(' ', '', ($user->name.'@'. $faker->freeEmailDomain)));
            $user->setPassword('password');

            $fixture->persist($user);
        }

       
        //create 30 emplacement
         for ($i = 1; $i < 31; $i++) {
            $type = $this->getReference('type_'. 1);

            $emplacement = new Property();
            $emplacement->setname('Emplacement '.$i);
            $emplacement->setType($type);
            $emplacement->setprice(mt_rand(12, 14));
            $emplacement->setOwner($admin);
            $emplacement->setImgUrl('https://source.unsplash.com/featured/?campsite+location');
            $emplacement->setReserved(mt_rand(0, 1));

            $fixture->persist($emplacement);
        }

        //create 30 Private mobile home
        for ($i = 1; $i < 31; $i++) {
            $type = $this->getReference('type_'. 3);

            $mobilehomeprivate = new Property();
            $mobilehomeprivate->setname('Mobile-Home '.$i);
            $mobilehomeprivate->settype($type);
            $mobilehomeprivate->setprice($faker->numberBetween(20, 24, 27, 34));
            $mobilehomeprivate->setOwner($proprietaire);
            $mobilehomeprivate->setImgUrl('https://source.unsplash.com/featured/?mobilehome');
            $mobilehomeprivate->setReserved(mt_rand(0, 1));

            $fixture->persist($mobilehomeprivate);
        }

        //create 20 mobile home
        for ($i = 31; $i < 51; $i++) {
            $type = $this->getReference('type_'. 3);

            $mobilehome = new Property();
            $mobilehome->setname('Mobile-Home '.$i);
            $mobilehome->setType($type);
            $mobilehome->setprice($faker->numberBetween(20, 24, 27, 34));
            $mobilehome->setOwner($admin);
            $mobilehome->setImgUrl('https://source.unsplash.com/featured/?caravan');
            $mobilehome->setReserved(mt_rand(0, 1));

            $fixture->persist($mobilehome);
        }

        //create 10 caravane
        for ($i = 1; $i < 11; $i++) {
            $type = $this->getReference('type_'. 2);

            $caravane = new Property();
            $caravane->setname('caravane '.$i);
            $caravane->setType($type);
            $caravane->setprice($faker->numberBetween(15, 18, 24));
            $caravane->setOwner($admin);
            $caravane->setImgUrl('https://source.unsplash.com/featured/?caravan');
            $caravane->setReserved(mt_rand(0, 1));

            $fixture->persist($caravane);
        }

        $fixture->flush();
    }

    public function getDependencies()
    {
        return [
            TypeFixtures::class
        ];
    }
}
