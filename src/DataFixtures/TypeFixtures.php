<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Type;
use App\Entity\User;
use App\Entity\Property;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class TypeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $types = [
            1 => ['name' => 'Emplacement'],
            2 => ['name' => 'Caravane'],
            3 => ['name' => 'Mobile-home'],
        ];

        foreach ($types as $key => $value){
            $type = new Type();
            $type->setType($value['name']);
            $manager->persist($type);

            $this->addReference('type_'. $key, $type);
        }

        $manager->flush();
    }
}