<?php

namespace App\Controller\Admin;

use App\Entity\Role;
use App\Entity\Type;
use App\Entity\User;
use App\Entity\Billing;
use App\Entity\Booking;
use App\Entity\Property;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();
        $url = $routeBuilder->setController(UserCrudController::class)->generateUrl();
        
        return $this->redirect($url);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Td Symfony React');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoRoute('Back to the website', 'fas fa-home', 'homepage');
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('Locations', 'fas fa-home', Property::class);
        yield MenuItem::linkToCrud('Réservations', 'fas fa-list-alt', Booking::class);
        yield MenuItem::linkToCrud('Factures', 'fas fa-file-invoice-dollar', Billing::class);
        yield MenuItem::linkToCrud('Type de locations', 'fas fa-home', Type::class);

    }
}
