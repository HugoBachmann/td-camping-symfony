<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\PropertyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PropertyRepository::class)
 * @ApiResource(
 *   collectionOperations={"get"={"normalization_context"={"groups"="property"}}},
 *   itemOperations={"get"={"normalization_context"={"groups"="property"}}},
 *   paginationEnabled=false
 * )
 */
class Property
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="properties")
     * @Groups({"property"})
     */
    private $owner;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"property"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"property"})
     */
    private $img_url;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"property"})
     */
    private $Reserved;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"property"})
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Type::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"property"})
     */
    private $Type;

    public function __construct()
    {
        $this->Role = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImgUrl(): ?string
    {
        return $this->img_url;
    }

    public function setImgUrl(string $img_url): self
    {
        $this->img_url = $img_url;

        return $this;
    }

    public function getReserved(): ?bool
    {
        return $this->Reserved;
    }

    public function setReserved(bool $Reserved): self
    {
        $this->Reserved = $Reserved;

        return $this;
    }

    public function __toString()
    {
        return $this->owner;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->Type;
    }

    public function setType(?Type $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

}
