import {render} from 'react-dom'
import React, { useEffect } from 'react'
import { useProperty } from './hooks'

function Properties () {
    const {items : properties, load : load, loading: loading} = useProperty('/api/properties')

    useEffect(() => {
        load()
    }, [])

    return <div class="row">
        {loading && 'loading ...'}
        {properties.map(p => <Property key={p.id} property={p} />)}
    </div>
}

function Property ({property}) {
    return <div class="col-12 col-md-6 col-lg-4">
        <h4>{property.name}</h4>
        <div className="imgContainer">
            <img src={property.img_url} alt="Property picture" />
        </div>
        <p>{property.price} €</p>
    </div>
}

class PropertyElement extends HTMLElement {
    
    connectedCallback () {
        render(<Properties/>, this)
    }
}

customElements.define('properties-element', PropertyElement)
